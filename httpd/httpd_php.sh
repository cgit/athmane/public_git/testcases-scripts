#!/bin/sh
# A small Apache HTTPd test suite
# Author: Athmane Madjoudj <athmane@fedoraproject.org>

echo "<?php echo phpinfo(); ?>" > /var/www/html/test.php

echo "Running $0 - httpd handle PHP test"

curl -s http://localhost/test.php | grep 'PHP Version' > /dev/null 2>&1

