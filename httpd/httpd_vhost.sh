#!/bin/sh
# A small Apache HTTPd test suite
# Author: Athmane Madjoudj <athmane@fedoraproject.org>

echo "Running $0 - httpd virtual host test."

echo "127.0.0.1   test" >>  /etc/hosts
cat > /etc/httpd/conf.d/vhost-test.conf <<EOF
NameVirtualHost *:80

<VirtualHost *:80>
 ServerAdmin webmaster@test
 DocumentRoot /var/www/vhosts/test/
 ServerName test
</VirtualHost>
EOF

mkdir -p /var/www/vhosts/test/
echo "Virtual Host Test Page" > /var/www/vhosts/test/index.html
service httpd reload
curl -s http://test/ | grep 'Virtual Host Test Page' > /dev/null 2>&1

