#!/bin/sh
# A small Apache HTTPd test suite
# Author: Athmane Madjoudj <athmane@fedoraproject.org>

# Install some required utilities
yum -y -d0 install curl grep

# Run install
./0-install_httpd.sh

# Run tests

for testscript in httpd_*
do
    ./$testscript
    [ $? -eq 0 ] && echo "==> [ PASS ]" || echo "==> [ FAIL ]"
done
