#!/bin/sh
# A small Apache HTTPd test suite
# Author: Athmane Madjoudj <athmane@fedoraproject.org>

echo "Running $0 - httpd basic authentication test."

cat > /etc/httpd/conf.d/dir-test-basic-auth.conf <<EOF
Alias /basic_auth_test /var/www/html/basic_auth_test
<Directory "/var/www/html/basic_auth_test">
  AuthType Basic
  AuthName "Test"
  AuthUserFile /etc/httpd/htpasswd
  require user test
</Directory>
EOF

htpasswd -c -b /etc/httpd/htpasswd test test
mkdir -p /var/www/html/basic_auth_test
echo "Basic authentication Test Page" > /var/www/html/basic_auth_test/index.html
service httpd reload
curl -s -u test:test http://localhost/basic_auth_test/ | grep 'Basic authentication Test Page' > /dev/null 2>&1

