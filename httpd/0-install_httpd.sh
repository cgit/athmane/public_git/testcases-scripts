#!/bin/bash
# A small Apache HTTPd test suite
# Author: Athmane Madjoudj <athmane@fedoraproject.org>

# HTTPD / PHP 
yum -y -d0 install httpd mod_ssl php php-mysql 
chkconfig httpd on
service httpd start
