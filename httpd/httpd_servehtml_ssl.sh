#!/bin/sh
# A small Apache HTTPd test suite
# Author: Athmane Madjoudj <athmane@fedoraproject.org>

echo "Running $0 - httpd serve html page test over SSL."

curl -ks https://localhost/ | grep 'Test Page' > /dev/null 2>&1

